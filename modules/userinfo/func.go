package userinfo

import (
	"gitlab.com/alexia.shaowei/sw.webframe.shell/openvar"
)

func CheckRoleHasPageActionPrivilege(roleID int, pageActionCode string) (bool, error) {
	dbResp, err := openvar.DB.Get("game_manage").Query(`SELECT power.code FROM game_manage.rp_role  AS role, game_manage.rp_power AS power WHERE role.id = ? AND power.code = ? AND power.status = 1 AND IF(role.isagent = 1, power.isAgent = 1, TRUE) AND INSTR(CONCAT(',', role.powerlist, ','), CONCAT(',', power.id, ',')) LIMIT 1;`, roleID, pageActionCode)

	if err != nil {
		return false, err
	}

	return dbResp.Length > 0, nil
}
