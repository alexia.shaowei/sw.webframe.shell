package userinfo

type RoleIDEnum int

const (
	ENUM_ROLE_ID_ADMIN               RoleIDEnum = 1
	ENUM_ROLE_ID_AGENT               RoleIDEnum = 4
	ENUM_ROLE_ID_SINGLE_WALLET_AGENT RoleIDEnum = 5
)

func (ref RoleIDEnum) Index() int {
	return int(ref)
}

func (ref RoleIDEnum) String() string {
	switch ref {
	case ENUM_ROLE_ID_ADMIN:
		return "Admin"

	case ENUM_ROLE_ID_AGENT:
		return "Agent"

	case ENUM_ROLE_ID_SINGLE_WALLET_AGENT:
		return "SingleWalletAgent"

	default:
		return "Undefined"
	}
}
