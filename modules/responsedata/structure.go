package responsedata

// RespDataABS ...
type RespDataABS struct {
	Success      bool        `json:"success"`
	ErrCode      int         `json:"errCode"`
	ErrMsg       string      `json:"errMsg"`
	ResponseData interface{} `json:"data"`
}
