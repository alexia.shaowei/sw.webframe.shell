package main

import (
	"fmt"
	"time"

	// jwt "gitlab.com/alexia.shaowei/submodule/swjwt"
	jwt "gitlab.com/alexia.shaowei/sw.webframe.shell/modules/swjwt"
)

type Token struct {
	Uid   int
	Name  string
	Price float32
	jwt.RegisteredClaimNames
}

func init() {
	accountToken = Token{
		Uid:   8787,
		Name:  "Gillain",
		Price: 12.56,

		RegisteredClaimNames: jwt.RegisteredClaimNames{
			ExpirationTime: time.Now().Add(time.Hour * 5).Unix(),
		},
	}

	signKey = []byte(`Gilla#HS156!@(!=\7)`)

	getTk()
}

var accountToken Token
var signKey []byte

func main() {
	fmt.Println("sw.jwt::main()")

	getClaim()
}

func getClaim() {
	signature := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVaWQiOjg3ODcsIk5hbWUiOiJHaWxsYWluIiwiUHJpY2UiOjEyLjU2LCJleHAiOjE2MzM0MzE0ODV9.3a5lPwDRIi5Jzi1UQ14PuixFKci_EkKh2N132Lw-H74"

	claims, err := jwt.Decrypt(signature, &Token{}, func(jtk *jwt.JWToken) (interface{}, error) {
		return signKey, nil
	})

	if err != nil {
		fmt.Printf("getClaim err: %v\n", err)
	}

	result, ok := claims.Claims.(*Token)
	if !ok {
		fmt.Println("convert error")
	}

	fmt.Println(result)

}

func getTk() {
	token := jwt.New(jwt.SigningHS256, accountToken)
	signature, err := token.Sign(signKey)
	if err != nil {
		fmt.Printf("getTk err: %v\n", err)
	}

	fmt.Println(signature)
}
