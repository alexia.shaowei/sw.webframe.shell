package gojwt

import "sync"

var signatureOpeMap = map[string]func() SignatureOperate{}
var signatureOpeLock = new(sync.RWMutex)

func putSignatureOpeMap(cryptoName string, signFunc func() SignatureOperate) {
	signatureOpeLock.Lock()
	defer func() {
		// fmt.Println()
		signatureOpeLock.Unlock()
	}()

	signatureOpeMap[cryptoName] = signFunc
}

func getSignatureOpeMap(cryptoName string) (signOpe SignatureOperate) {
	signatureOpeLock.RLock()
	defer func() {
		// fmt.Pritln()
		signatureOpeLock.RUnlock()
	}()

	if signFunc, ok := signatureOpeMap[cryptoName]; ok {
		signOpe = signFunc()
	}

	return
}
