module gitlab.com/alexia.shaowei/sw.webframe.shell

go 1.16

require (
	github.com/go-yaml/yaml v2.1.0+incompatible
	gitlab.com/alexia.shaowei/rotatelogs v0.0.0
	gitlab.com/alexia.shaowei/sw.webframe.core v0.0.1
	gitlab.com/alexia.shaowei/swmongo v0.0.0
	gitlab.com/alexia.shaowei/swmysql v0.0.2-0.20211101032922-4ed404af1de2
	gitlab.com/alexia.shaowei/swredis v0.0.0
)
