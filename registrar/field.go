package registrar

const (
	ALL     uint8 = 0
	POST    uint8 = 1
	GET     uint8 = 2
	DEL     uint8 = 3
	PUT     uint8 = 4
	OPTIONS uint8 = 5
)

// Controller ...
var Controller Router
