package controller

import (
	reg "gitlab.com/alexia.shaowei/sw.webframe.shell/registrar"
	api "gitlab.com/alexia.shaowei/sw.webframe.shell/zygote/service/jwt"
)

func init() {

	reg.Controller.AddRouter(reg.GET, "/jwt", api.Auth, reg.Option{VerifyAuth: true})
	reg.Controller.AddRouter(reg.POST, "/jwt", api.CreateJWT)
	reg.Controller.AddRouter(reg.GET, "/white/list", api.WhiteList)
	reg.Controller.AddRouter(reg.GET, "/ServInfo", api.ServInfo)

	reg.Controller.AddRouter(reg.GET, "/ServInfo", api.ServInfoOpts, reg.Option{VerifyAuth: true, VerifyPrivilegeActionCode: "xxx"})
}
