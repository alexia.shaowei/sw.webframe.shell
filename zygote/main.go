package main

import (
	"gitlab.com/alexia.shaowei/sw.webframe.shell/server"
	_ "gitlab.com/alexia.shaowei/sw.webframe.shell/zygote/controller"
)

func main() {

	server.Deploy().Run()
}
