package server

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"runtime/debug"

	"gitlab.com/alexia.shaowei/sw.webframe.shell/modules/responsedata"
	u "gitlab.com/alexia.shaowei/sw.webframe.shell/modules/userinfo"
	"gitlab.com/alexia.shaowei/sw.webframe.shell/openvar"
	"gitlab.com/alexia.shaowei/sw.webframe.shell/registrar"

	core "gitlab.com/alexia.shaowei/sw.webframe.core"
)

type WebServer struct {
	Ws         *core.Multiplexer
	serverConf websiteCfg
}

func (ref *WebServer) Run() {
	routerSlice := registrar.Controller.GetRouter()

	for i := 0; i < len(routerSlice); i++ {
		val := routerSlice[i]
		method := val.Method
		callback := val.Callback
		optional := val.Optional

		var handlerFn http.HandlerFunc = func(resp http.ResponseWriter, req *http.Request) {

			defer func() {
				if err := recover(); err != nil {
					openvar.Log.Panic("%v:%v", err, string(debug.Stack()))
					resp.WriteHeader(http.StatusInternalServerError)
					resp.Write(responsedata.CodeResponseFailure(responsedata.ERROR_CODE_HTTP_REQ_PANIC, responsedata.ERROR_MSG_HTTP_REQ_PANIC))
					return
				}
			}()

			resp.Header().Set(ACCESS_CONTROL_ALLOW_ORIGIN, STAR)
			resp.Header().Set(ACCESS_CONTROL_ALLOW_HEADERS, STAR)

			var pReq *registrar.ParamReq = &registrar.ParamReq{
				RESTMap:   make(map[string]string),
				URIParam:  make(map[string][]string),
				FormParam: make(map[string][]string),
				Resp:      nil,
				Req:       nil,
			}

			restMap := core.URLReqParamMap(req)
			if len(restMap) > 0 {
				pReq.RESTMap = restMap
			}

			req.ParseForm()

			for uriParamKey, uriParamValue := range req.Form {
				pReq.URIParam[uriParamKey] = uriParamValue
			}

			for formParamKey, formParamValue := range req.PostForm {
				pReq.FormParam[formParamKey] = formParamValue
				delete(pReq.URIParam, formParamKey)
			}

			var userinfo *u.UserInfo = nil

			if optional.VerifyAuth {
				token := req.Header.Get(PARAM_JWT_HEADER)

				if len(token) == 0 {
					resp.Write(responsedata.CodeResponseFailure(responsedata.ERROR_CODE_ACCOUNT_AUTH_PARAM_MISSING, responsedata.ERROR_MSG_ACCOUNT_AUTH_PARAM_MISSING))
					return
				}

				var errCode int = responsedata.ERROR_CODE_NONE
				var errMsg string = responsedata.ERROR_MSG_NONE
				var refreshToken = EMPTY
				userinfo, refreshToken, errCode, errMsg = verifyAuthToken(token)

				if errCode != responsedata.ERROR_CODE_NONE {
					resp.Write(responsedata.CodeResponseFailure(errCode, errMsg))
					return
				}

				if userinfo == nil {
					resp.Write(responsedata.CodeResponseFailure(responsedata.ERROR_CODE_ACCOUNT_AUTH_UNKNOWNUSER, responsedata.ERROR_MSG_ACCOUNT_AUTH_UNKNOWNUSER))
					return
				}

				resp.Header().Set(PARAM_JWT_HEADER, refreshToken)

				if len(optional.VerifyPrivilegeActionCode) != 0 {
					hasPrivilege, err := u.CheckRoleHasPageActionPrivilege(userinfo.RoleID, optional.VerifyPrivilegeActionCode)
					if err != nil {
						openvar.Log.Error("verify privilege err: %v", err)
						resp.Write(responsedata.CodeResponseFailure(responsedata.ERROR_CODE_DB_SYSTEM, responsedata.ERROR_MSG_DB_SYSTEM))
						return
					}

					if !hasPrivilege {
						resp.Write(responsedata.CodeResponseFailure(responsedata.ERROR_CODE_PERMISSION_DENIED_PRIVILEGEACT, responsedata.ERROR_MSG_PERMISSION_DENIED_PRIVILEGEACT))
						return
					}
				}
			}

			if optional.BodyContent {
				if req.Body != nil {
					bt, err := ioutil.ReadAll(req.Body)
					if err != nil {
						pReq.BodyContent = EMPTY
					}

					pReq.BodyContent = string(bt)

					req.Body.Close()
				}
			}

			if len(optional.ContentType) > 0 {
				resp.Header().Set(CONTENT_TYPE, optional.ContentType)
			}

			servinfo := &registrar.ServInfo{
				Version:  ftserverConfig.Serv.Version,
				Host:     removeAddrPort(req.Host),
				RemoteIP: removeAddrPort(req.RemoteAddr),
				Method:   req.Method,
				User:     userinfo,
			}

			if optional.CustomizedHandle {
				pReq.Resp = resp
				pReq.Req = req

				callback(pReq, servinfo)

				return
			}

			resp.Write(callback(pReq, servinfo))
		}

		switch method {
		case 0:
			ref.Ws.All(val.URI, handlerFn)

		case 1:
			ref.Ws.Post(val.URI, handlerFn)

		case 2:
			ref.Ws.Get(val.URI, handlerFn)

		case 3:
			ref.Ws.Del(val.URI, handlerFn)

		case 4:
			ref.Ws.Put(val.URI, handlerFn)

		case 5:
			ref.Ws.Options(val.URI, handlerFn)

		default:
			log.Println("No support method")
		}
	}

	log.Printf("server version: %s", ftserverConfig.Serv.Version)
	log.Print("server start up ...")
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", ref.serverConf.Serv.Port), ref.Ws))
}
