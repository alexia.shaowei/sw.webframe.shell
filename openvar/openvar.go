package openvar

import (
	"gitlab.com/alexia.shaowei/rotatelogs"
	"gitlab.com/alexia.shaowei/swmongo"
	"gitlab.com/alexia.shaowei/swmysql"
	"gitlab.com/alexia.shaowei/swredis"
)

func init() {
	WhiteList = make([]string, 0)

	DB = &DBContainer{
		dbls: make(map[string]*swmysql.DBOperator),
	}

}

var Log *rotatelogs.RotateLog
var Mongo *swmongo.MongoOperator
var Cache *swredis.SWRedis
var DB *DBContainer

var WhiteList []string
